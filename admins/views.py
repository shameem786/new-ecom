from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.core import serializers

# Create your views here.
import datetime
from ecommerce.settings import ADMIN_LOGIN_REDIRECT_URL

from rest_framework.parsers import JSONParser

from .serializers import *
from store.models import Order,OrderItem,Courier,OrderStatus,Catogory,Product,Capacity


from .forms import *
from .decorators import superuser_login_required,saleperson_login_required


def admin_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                if user.is_staff:
                    if user.is_superuser:
                        login(request, user)
                        return redirect('superuser_home')
                    elif user.is_staff and user.user_type=="S":
                        login(request, user)
                        return redirect('sales_home')
                    else:
                        print(user.user_type)
                        print(user.is_staff)
                        print("This section")# redirect by using the user types
                        pass
                else:
                    messages.error(request, "Please ensure you are a staff")
            else:
                messages.error(request, "Invalid email or password.")
        else:
            messages.error(request, "Invalid email or password.")
            # print("Invalid form")
    form = AuthenticationForm()
    return render(request=request,
                  template_name="login.html",
                  context={"form": form})

def logout_view(request):
    logout(request)
    messages.success(request, "Logged out successfully!")
    return redirect("admin_login")


@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def superuser_home(request):
    return render(request, 'superuser_home.html')



@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def add_staff(request):
    if request.method == 'POST':
        form = AddStaff(request.POST)

        if form.is_valid():
            user=form.save()
            user.is_staff=True
            user.password=make_password(user.password)
            user.save()
            return redirect('superuser_home')
    else:
        form = AddStaff()
    return render(request, 'registration.html', {'form': form})



@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def add_product(request):
    if request.method == 'POST':
        form = AddProduct(request.POST,request.FILES)

        if form.is_valid():
            form.save()

            return redirect('superuser_home')
    else:
        form = AddProduct()
    return render(request, 'add_product.html', {'form': form})


@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def sales_home(request):
    return render(request,'sales_home.html')


@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def sales_orders(request):
    orders=Order.objects.filter(complete=True,status=1).order_by('date_ordered')
    print(orders)
    context={'orders':orders}
    return render(request,'sales_order.html',context)


@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def sales_orders_detail(request,pk):
    order=Order.objects.get(id=pk)
    items=OrderItem.objects.filter(order=pk)
    couriers=Courier.objects.all()
    #print(order)
    context={'items':items,'order':order,'couriers':couriers}
    return render(request,'sales_order_detail.html',context)

@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def billed_orders(request):
    orders=Order.objects.filter(complete=True,status=2).order_by('date_ordered')
    #print(orders)
    context={'orders':orders}
    return render(request,'billed_order.html',context)

@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def billed_orders_detail(request,pk):
    order=Order.objects.get(id=pk)
    items=OrderItem.objects.filter(order=pk)
    couriers=Courier.objects.all()
    pkd_date=OrderStatus.objects.get(order=order,status="Packed and dispatched to courier").date_added
    #print(order)
    context={'items':items,'order':order,'couriers':couriers,'date':pkd_date}
    return render(request,'billed_order_detail.html',context)


def courier_allocate(request, pk):
    try:
        order = Order.objects.get(pk=pk)
    except Order.DoesNotExist:
        return HttpResponse(status=404)
    if request.method == 'POST':
        courier_id=request.POST['courier_id']
        #for i in request.POST:
            #print(i)
        product_list=request.POST.getlist('product_list[]')
        quantity_list = request.POST.getlist('quantity_list[]')
        for i in range(0,len(product_list)):
            item=OrderItem.objects.get(order=pk,product=product_list[i])
            item.quantity=quantity_list[i]
            item.save()
        courier=Courier.objects.get(id=courier_id)
        order.courier=courier
        order.status=2
        OrderStatus.objects.create(order=order,status="Packed and dispatched to courier")
        order.save()
        return HttpResponse('')


@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def our_product(request):
    catogory=Catogory.objects.all()
    context={'catogory':catogory}
    return render(request,'catogory.html',context)


@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def products_catogory(request,name):
    catogory=Catogory.objects.get(name=name)
    prods=Product.objects.filter(catogory=catogory)
    context={'products':prods,'catogory':name}
    return render(request,'products_catogory.html',context)


@superuser_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def products_details(request,name,pk):
    if request.method=='POST':
        prod=Product.objects.get(id=pk)
        context={'product':prod}
        return render(request,'products_detail.html',context)

@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def orders_by_date(request):
    if request.method=='POST':
        d = request.POST['date']
        order_list=[]
        orders = Order.objects.filter(date_ordered__startswith=d)

        for i in orders:
            order_list.append(i.id)
        #print(order_list)
        return JsonResponse(order_list,safe=False)

    return render(request,'orders_by_date.html')


def orders_by_date_detail(request,pk):
    if request.method == 'POST':
        order=Order.objects.get(id=pk)
        items=OrderItem.objects.filter(order=order)
        print(order)
        return render(request,'billed_order_detail.html',{'order':order,'items':items})


@saleperson_login_required(login_url=ADMIN_LOGIN_REDIRECT_URL)
def capacity(request):
    if request.method=='POST':
        values=request.POST.getlist('values[]')
        #print(values)
        days=['SUN','MON','TUE','WED','THU','FRI','SAT']
        for i in range(0,7):
            a=Capacity.objects.get(day=days[i])
            a.no_deliver=int(values[i])
            a.save()
        return JsonResponse('', safe=False)
    else:
        capacity=Capacity.objects.all()
        context={'capacity':capacity}
        return render(request,'set_capacity.html',context)



