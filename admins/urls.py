from django.conf.urls.static import static
from django.urls import path
from . import views
from django.conf import settings
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin1/', views.admin_login, name='admin_login'),
    path('superuser/', views.superuser_home, name='superuser_home'),
    path('superuser/add_staff', views.add_staff, name='add_staff'),
    path('superuser/add_product', views.add_product, name='add_product'),
    path('superuser/select_catogory', views.our_product, name='catogory'),
    path('superuser/select_catogory/<str:name>', views.products_catogory),
    path('superuser/<str:name>/<int:pk>', views.products_details),
    path('sales/', views.sales_home, name='sales_home'),
    path('sales/orders', views.sales_orders, name='sales_order'),
    path('sales/orders/<int:pk>', views.sales_orders_detail),
    path('courier_allocate/<int:pk>',views.courier_allocate),
    path('sales/billed_orders', views.billed_orders, name='billed_order'),
    path('sales/billed_orders/<int:pk>', views.billed_orders_detail),
    path('sales/orders_by_date/select_date', views.orders_by_date, name='orders_by_date'),
    path('sales/orders_by_date/select_date/<int:pk>', views.orders_by_date_detail),
    path('sales/set_deliver_capacity', views.capacity,name='set_capacity'),
    path('admin_logout/', views.logout_view, name='admin_logout'),

  ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)