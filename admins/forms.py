from django import forms
from store.models import *
from .models import User


class AddStaff(forms.ModelForm):

   class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'mobile_no', 'user_type','password')

class AddProduct(forms.ModelForm):

   class Meta:
        model = Product
        fields = ('name', 'brand', 'catogory', 'product_info', 'price','stock','image1')