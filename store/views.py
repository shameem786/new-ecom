from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from django.shortcuts import render,redirect
from django.http import JsonResponse, HttpResponse
import json
import datetime

from ecommerce.settings import LOGIN_REDIRECT_URL
from .models import*
from .utils import cookieCart, cartData,guestOrder,CookieToDb,dispatch_date
from .forms import *
from django.contrib.auth import login, authenticate, logout
from django.core import serializers


# Create your views here.


def home(request):
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']

    catogory = Catogory.objects.all()
    #print(catogory)
    #print(catogory)
   # show_category()

    context = {'catogory': catogory,
               'cartItems': cartItems}
    return render(request, 'store/home.html', context)


def login_view(request):
    if request.method == 'POST':
        form = CustomAuthForm(request=request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect('store')
            else:
                messages.error(request, "Invalid email or password.")
                print("Not none")
        else:
            messages.error(request, "Invalid email or password.")
            print("Invalid form")
    form = CustomAuthForm()
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']
    catogory = Catogory.objects.all()



    return render(request=request,
                  template_name="store/login.html",
                  context = {'catogory': catogory, 'cartItems': cartItems, 'items': items,"form": form})

def logout_view(request):
    logout(request)
    #messages.info(request, "Logged out successfully!")
    return redirect("store")


def register_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)
            return redirect('store')
    else:
        form = SignUpForm()
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']
    catogory = Catogory.objects.all()
    return render(request, 'store/registration.html', {'form': form,'catogory': catogory, 'cartItems': cartItems, 'items': items})





def store(request,id,name):
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']



    prods=Product.objects.filter(catogory_id=id)

    print(prods)

    cart_checking=[]  #for orders,checking that it is in cart

    if not request.user.is_authenticated:
        for i in items:
            it={
                'p_id':i['product']['id'],
                'quantity':i['quantity'],
            }
            cart_checking.append(it)

    else:
        for i in items:
            it={
                'p_id': i.product.id,
                'quantity': i.quantity,
            }
            cart_checking.append(it)
    #print(cart_checking)

    for product in prods:
        for ca in cart_checking:
            product.match_status=0
            product.match_quantity=0
            if ca['p_id']==product.id:  #checking match
                product.match_status = 1
                product.match_quantity = ca['quantity']
                break

    catogory = Catogory.objects.all()

    context ={'catogory': catogory,'products':prods,'cartItems':cartItems,'items':items,'head':name,'count':prods.count()}
    return render(request,'store/store.html',context)






def view_item(request,name,pk):
    data = cartData(request)

    cartItems = data['cartItems']

    product=Product.objects.get(id=pk,name=name)
    catogory = Catogory.objects.all()

    context={'catogory': catogory,'product':product,'cartItems':cartItems}
    return render(request,'store/view_item.html',context)

#cart page
def cart(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    catogory = Catogory.objects.all()


    context ={'catogory': catogory,'items':items,'order':order,'cartItems':cartItems}
    return render(request,'store/cart.html',context)

@login_required(login_url=LOGIN_REDIRECT_URL)
def checkout(request):
    data=cartData(request)

    cartItems=data['cartItems']
    order=data['order']
    items=data['items']
    address=UserAddress.objects.filter(user=request.user)
    catogory = Catogory.objects.all()
    context = {'catogory': catogory,'items': items, 'order': order,'cartItems':cartItems,"address":address}
    return render(request,'store/checkout.html',context)

@login_required(login_url=LOGIN_REDIRECT_URL)
def confirm_order(request,order_id):
    data=cartData(request)

    cartItems=data['cartItems']
    order=data['order']
    items=data['items']
    #address=UserAddress.objects.filter(user=request.user)
    catogory = Catogory.objects.all()
    confirm_order=Order.objects.get(id=order_id)
    confirm_items=confirm_order.orderitem_set.all()
    print(confirm_items)
    shipping=ShippingAddress.objects.get(order_id=order_id)
    billing=BillingAddress.objects.get(order_id=order_id)
    context = {'catogory': catogory,'items': items, 'order': order,'cartItems':cartItems,"shipping":shipping,'billing':billing,"confirm_items":confirm_items}
    return render(request,'store/order_confirm.html',context)

def updateItem(request):
    data=json.loads(request.body)
    productId=data['productId']
    action= data['action']

    #print(action)
    #print(productId)

    user=request.user
    product=Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(user=user, complete=False)

    orderItem,created=OrderItem.objects.get_or_create(order=order,product=product)

    if action == 'add':
        orderItem.quantity = (orderItem.quantity +1)
    elif action == 'remove':
        orderItem.quantity = (orderItem.quantity - 1)

    orderItem.save()

    order_item_quantity=orderItem.quantity   #assigning value to variable before deleting
    item_total=orderItem.get_total     #total  of this perticular  order
    if orderItem.quantity <=0:
        orderItem.delete()
    #print(order.courier)
    data={
        'number_order':order.get_cart_items,'total':order.get_cart_total,'item_quantity':order_item_quantity,'p_id':productId,'get_total':item_total
    }


    return JsonResponse(data,safe=False)


def processOrder(request):
    transaction_id=datetime.datetime.now().timestamp()
    data=json.loads(request.body)

    if request.user.is_authenticated:
        user=request.user
        order, created = Order.objects.get_or_create(user=user, complete=False)
    else:
        user,order=guestOrder(request,data)

    total = float(data['form']['total'])
    order.transaction_id = transaction_id
    if total == float(order.get_cart_total):
        order.complete = True
        order.status=1
        order.order_complete=datetime.datetime.now()
        dispatch_dat=dispatch_date()
        order.dispatch_date=dispatch_dat
        OrderStatus.objects.create(order=order,status="Order Placed")
        OrderStatus.objects.create(order=order,status="Expected dispatch", date_assign=dispatch_dat)
    order.save()

    if order.shipping == True:
        ShippingAddress.objects.create(
            user=user,
            order=order,
            first_name=data['shipping']['first_name'],
            last_name=data['shipping']['last_name'],
            company_name=data['shipping']['company_name'],
            phone=data['shipping']['number'],
            email=data['shipping']['email'],
            state=data['shipping']['state'],
            address_1=data['shipping']['add1'],
            address_2=data['shipping']['add2'],
            city=data['shipping']['city'],
            district=data['shipping']['district'],
            zipcode=data['shipping']['zip'],
        )
        BillingAddress.objects.create(
            user=user,
            order=order,
            first_name=data['shipping']['first_name'],
            last_name=data['shipping']['last_name'],
            company_name=data['shipping']['company_name'],
            phone=data['shipping']['number'],
            email=data['shipping']['email'],
            state=data['shipping']['state'],
            address_1=data['shipping']['add1'],
            address_2=data['shipping']['add2'],
            city=data['shipping']['city'],
            district=data['shipping']['district'],
            zipcode=data['shipping']['zip'],
        )
        #useraddress,create=UserAddress.objects.get_or_create(user=user,address=data['shipping']['address'],city=data['shipping']['city'],
                                                             #state=data['shipping']['state'],zipcode=data['shipping']['zipcode'])
        #print(create)
    data = {
        'order_id': order.id
    }
    return JsonResponse(data,safe=False)


@login_required(login_url=LOGIN_REDIRECT_URL)
def addaddress(request):
    if request.method == 'POST':
        form = AddAddressForm(request.POST)
        if form.is_valid():
            if request.POST.get('defaul', False):
                try:
                    print("Inside try")
                    a=UserAddress.objects.get(user=request.user,defaul=True)
                    a.defaul=False
                    a.save()
                except:
                    print("Inside except")
                    pass
            add=form.save()
            add.user=request.user
            add.save()
            return redirect('addaddress')
    else:
        form=AddAddressForm()
        try:
            address=UserAddress.objects.filter(user=request.user)
        except:
            address=None

        data = cartData(request)
        cartItems = data['cartItems']

        prods = Product.objects.all()
        catogory = Catogory.objects.all()

        return render(request=request,
                  template_name="store/addaddress.html",
                  context={"form": form,"address":address,'products': prods, 'cartItems': cartItems,'catogory':catogory})


@login_required(login_url=LOGIN_REDIRECT_URL)
def change_address(request,pk):

    data = cartData(request)
    cartItems = data['cartItems']

    add=UserAddress.objects.get(id=pk)
    catogory = Catogory.objects.all()


    context={'cartItems': cartItems,'address':add,'catogory':catogory}
    return render(request,'store/address_show.html',context)


@login_required(login_url=LOGIN_REDIRECT_URL)
def change_address_done(request,pk):
    if request.method=='POST':
        first_name=request.POST['first_name']
        last_name = request.POST['last_name']
        company_name = request.POST['company_name']
        phone = request.POST['phone']
        email = request.POST['email']
        address_2=request.POST['address1']
        address_1=request.POST['address1']
        city=request.POST['city']
        district = request.POST['district']
        state=request.POST['state']
        zip=request.POST['zipcode']
        check = request.POST.get('check',False)
        if check:
            check=True
            u_add=UserAddress.objects.get(user=request.user,defaul=True)
            if u_add.id==pk:
                pass
            else:
                u_add.defaul=False
                u_add.save()
        add=UserAddress.objects.get(id=pk)
        add.first_name=first_name
        add.last_name=last_name
        add.company_name=company_name
        add.phone=phone
        add.email=email
        add.address_1=address_1
        add.address_2=address_2
        add.city=city
        add.district=district
        add.state=state
        add.zipcode=zip
        add.defaul=check
        add.save()


        return redirect('addaddress')






def delete_address(request):
    if request.method == 'POST':
        address_id=request.POST['address_id']
        ad=UserAddress.objects.get(id=address_id)
        ad.delete()
        return HttpResponse('')


@login_required(login_url=LOGIN_REDIRECT_URL)
def my_orders(request):
    data = cartData(request)
    cartItems = data['cartItems']

    prods = Product.objects.all()


    orders=Order.objects.filter(user=request.user,complete=True)
    catogory = Catogory.objects.all()
    #print(orders)

    context={'orders':orders,'products':prods,'cartItems':cartItems,'catogory':catogory}
    return render(request,"store/myorders.html",context)

@login_required(login_url=LOGIN_REDIRECT_URL)
def track_order(request,pk):
    if request.method=="POST":
        data = cartData(request)
        cartItems = data['cartItems']

        prods = Product.objects.all()

        items = OrderItem.objects.filter(order=pk)
        status=OrderStatus.objects.filter(order=pk)
        shipping=ShippingAddress.objects.get(order_id=pk)
        billing=BillingAddress.objects.get(order_id=pk)
        catogory = Catogory.objects.all()
        context={'status':status,'products':prods,'cartItems':cartItems,'items':items,'catogory':catogory,'shipping':shipping,"billing":billing}
        return render(request,"store/track_order.html",context)

def redirect_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                CookieToDb(request,user=user)
                response= redirect('store')
                response.delete_cookie('cart')
                return response
            else:
                messages.error(request, "Invalid email or password.")
                print("Not none")
        else:
            messages.error(request, "Invalid email or password.")
            print("Invalid form")
    form = CustomAuthForm()
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']
    catogory = Catogory.objects.all()
    return render(request=request,
                  template_name="store/login.html",
                  context={"form": form,"text":"You must login to continue,if a new user please register",'catogory': catogory, 'cartItems': cartItems, 'items': items})




def redirect_register_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)
            CookieToDb(request, user=user)
            response = redirect('store')
            response.delete_cookie('cart')
            return response
    else:
        form = SignUpForm()
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']
    catogory = Catogory.objects.all()
    return render(request, 'store/registration.html', {'form': form,"text":"You must login to continue,if a new user please register",'catogory': catogory, 'cartItems': cartItems, 'items': items})


def logout_track(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    catogory = Catogory.objects.all()
    context = {'catogory': catogory, 'items': items, 'order': order, 'cartItems': cartItems}
    return render(request,'store/logout_track.html',context)


def id_mail_track(request):
    if request.method == 'POST':
        order_id=request.POST['order_id']
        email=request.POST['email']
        #print(email);
        #print(order_id)
        billing=BillingAddress.objects.filter(order_id=order_id,email=email)
        #print(billing)
        status = OrderStatus.objects.filter(order=order_id)
        data=serializers.serialize('json', status)
        #print(len(billing))
        if(len(billing)>0):
            return JsonResponse(data, safe=False)



def contact_us(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    catogory = Catogory.objects.all()
    context = {'catogory': catogory, 'items': items, 'order': order, 'cartItems': cartItems}
    return render(request, 'store/contact_us.html', context)



def about(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    catogory = Catogory.objects.all()
    context = {'catogory': catogory, 'items': items, 'order': order, 'cartItems': cartItems}
    return render(request, 'store/about.html', context)




def store_main_catogory(request,name,id):
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']

    all_catogory=Catogory.objects.filter(parent_id=id).values('id')
    print(all_catogory)



    prods=Product.objects.filter(catogory_id__in=all_catogory)

    print(prods)

    cart_checking=[]  #for orders,checking that it is in cart

    if not request.user.is_authenticated:
        for i in items:
            it={
                'p_id':i['product']['id'],
                'quantity':i['quantity'],
            }
            cart_checking.append(it)

    else:
        for i in items:
            it={
                'p_id': i.product.id,
                'quantity': i.quantity,
            }
            cart_checking.append(it)
    #print(cart_checking)

    for product in prods:
        for ca in cart_checking:
            product.match_status=0
            product.match_quantity=0
            if ca['p_id']==product.id:  #checking match
                product.match_status = 1
                product.match_quantity = ca['quantity']
                break

    catogory = Catogory.objects.all()

    context ={'catogory': catogory,'products':prods,'cartItems':cartItems,'items':items,'head':name,'count':prods.count()}
    return render(request,'store/store.html',context)



def deal_of_the_day(request):
    data = cartData(request)
    updateItem
    cartItems = data['cartItems']
    items = data['items']



    prods=Product.objects.all()

    #print(prods)

    cart_checking=[]  #for orders,checking that it is in cart

    if not request.user.is_authenticated:
        for i in items:
            it={
                'p_id':i['product']['id'],
                'quantity':i['quantity'],
            }
            cart_checking.append(it)

    else:
        for i in items:
            it={
                'p_id': i.product.id,
                'quantity': i.quantity,
            }
            cart_checking.append(it)
    #print(cart_checking)

    for product in prods:
        for ca in cart_checking:
            product.match_status=0
            product.match_quantity=0
            if ca['p_id']==product.id:  #checking match
                product.match_status = 1
                product.match_quantity = ca['quantity']
                break

    catogory = Catogory.objects.all()

    context ={'catogory': catogory,'products':prods,'cartItems':cartItems,'items':items,'head':"Deal of the Day",'count':prods.count()}
    return render(request,'store/store.html',context)


