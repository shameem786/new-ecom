from django import forms
from django.contrib.auth.forms import UserCreationForm
from admins.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, EmailInput

from .models import *


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True )
    email = forms.EmailField(max_length=254 )

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'mobile_no', 'password1', 'password2', )


class AddAddressForm(forms.ModelForm):
    class Meta:
        model = UserAddress
        fields = ('first_name','last_name','company_name','phone','email','address_1','address_2', 'city', 'district','state', 'zipcode', 'defaul' )


class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=EmailInput(attrs={'placeholder': 'Email'}))
    password = forms.CharField(widget=PasswordInput(attrs={'placeholder':'Password'}))