from django.conf.urls.static import static
from django.urls import path
from . import views
from django.conf import settings
from django.contrib.auth import views as auth_views

urlpatterns =[path('',views.home,name='store'),
    path('Catogory/<int:id>/<str:name>',views.store),
    path('Catogory/<str:name>/<int:id>',views.store_main_catogory),
    path('Catogory/deal_of_the_day',views.deal_of_the_day,name="deal_of_the_day"),
    path('cart/',views.cart,name='cart'),
    path('track_order/',views.logout_track,name='logout_track'),
    path('checkout/',views.checkout,name='checkout'),
    path('checkout/<int:order_id>/confirm',views.confirm_order),
    path('login/',views.login_view, name='login'),
    path("logout", views.logout_view, name="logout"),
    path('register/',views.register_view,name='register'),
    path('addaddress/',views.addaddress,name='addaddress'),
    path('myorders/',views.my_orders,name='myorders'),
    path('myorders/<int:pk>',views.track_order),
    path('product/<str:name>/<int:pk>/',views.view_item),
    path('login_to_continue/',views.redirect_login, name='redirect_login'),
    path('register_to_continue/',views.redirect_register_view, name='redirect_register'),
    path('address/change/<int:pk>',views.change_address),
    path('address/change/<int:pk>/done', views.change_address_done),
    path('contact_us/',views.contact_us,name='contact_us'),
    path('about/',views.about,name='about'),

    path('update_item/',views.updateItem,name='update_item'),
    path('process_order/',views.processOrder,name='process_order'),
    path('delete_address/',views.delete_address),
    path('id_email_track/',views.id_mail_track),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)