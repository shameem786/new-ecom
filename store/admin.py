from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from .models import *

# Register your models here.
class CategoryAdmin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title',
                    'related_products_count', 'related_products_cumulative_count')
    list_display_links = ('indented_title',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # Add cumulative product count
        qs = Catogory.objects.add_related_count(
                qs,
                Product,
                'catogory',
                'products_cumulative_count',
                cumulative=True)

        # Add non cumulative product count
        qs = Catogory.objects.add_related_count(qs,
                 Product,
                 'catogory',
                 'products_count',
                 cumulative=False)
        return qs

    def related_products_count(self, instance):
        return instance.products_count
    related_products_count.short_description = 'Related products (for this specific category)'

    def related_products_cumulative_count(self, instance):
        return instance.products_cumulative_count
    related_products_cumulative_count.short_description = 'Related products (in tree)'


admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(ShippingAddress)
admin.site.register(BillingAddress)
admin.site.register(Courier)
admin.site.register(UserAddress)
admin.site.register(OrderStatus)
admin.site.register(Catogory,CategoryAdmin)
admin.site.register(Capacity)



