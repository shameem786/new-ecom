from django.db import models
import os
import uuid
from django.utils.translation import ugettext_lazy as _

from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
from admins.models import User
from mptt.models import MPTTModel, TreeForeignKey



class Catogory(MPTTModel):
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    parent = TreeForeignKey('self', blank=True,on_delete=models.SET_NULL, null=True, related_name='children')
    image=models.ImageField(upload_to='catogory_images/')

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url

class Product(models.Model):
    name=models.CharField(max_length=200)
    price=models.DecimalField(max_digits=10,decimal_places=2)
    stock=models.IntegerField(default=0)
    digital=models.BooleanField(default=False,null=True,blank=True)
    catogory=models.ForeignKey(Catogory,on_delete=models.SET_NULL,null=True,blank=True)
    brand=models.CharField(max_length=50)
    image1=models.ImageField(upload_to='product_images/', null=True, blank=True)
    image2 = models.ImageField(upload_to='product_images/', null=True, blank=True)
    image3 = models.ImageField(upload_to='product_images/', null=True, blank=True)
    image4 = models.ImageField(upload_to='product_images/', null=True, blank=True)
    product_info=models.CharField(max_length=500)


    def __str__(self):
        return self.name

    @property
    def image1URL(self):
        try:
            url=self.image1.url
        except:
            url=''
        return url

    @property
    def image2URL(self):
        try:
            url = self.image2.url
        except:
            url = ''
        return url

    @property
    def image3URL(self):
        try:
            url = self.image3.url
        except:
            url = ''
        return url

    @property
    def image4URL(self):
        try:
            url = self.image4.url
        except:
            url = ''
        return url




class Courier(models.Model):
    courier_name=models.CharField(max_length=200,null=False)
    place=models.CharField(max_length=200,null=False)
    zipcode=models.CharField(max_length=200,null=False)


    def __str__(self):
        return self.courier_name

class Order(models.Model):
    user=models.ForeignKey(User,on_delete=models.SET_NULL,null=True,blank=True)
    date_ordered=models.DateTimeField(auto_now_add=True)
    complete=models.BooleanField(default=False)
    transaction_id=models.CharField(max_length=100,null=True)
    actual_price=models.DecimalField(default=0,max_digits=10,decimal_places=2)
    discount=models.DecimalField(default=0,max_digits=10,decimal_places=2)
    price=models.DecimalField(default=0,max_digits=10,decimal_places=2)
    status=models.IntegerField(default=0)
    courier = models.ForeignKey(Courier, on_delete=models.SET_NULL, null=True)
    order_complete=models.DateTimeField(null=True)
    dispatch_date=models.DateTimeField(null=True)

    def __str__(self):
        return str(self.id)

    @property
    def shipping(self):
        shipping=False
        orderItems = self.orderitem_set.all()
        for i in orderItems:
            if i.product.digital == False:
                shipping=True
        return shipping

    @property
    def get_cart_total(self):
        orderitems=self.orderitem_set.all()
        total=sum([item.get_total for item in orderitems])
        #print("Total:",total)
        return total

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])
        return total

class OrderItem(models.Model):
    product=models.ForeignKey(Product,on_delete=models.SET_NULL,null=True)
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    quantity=models.IntegerField(default=0,null=True,blank=True)
    date_added=models.DateTimeField(auto_now_add=True)

    @property
    def get_total(self):
        total=self.product.price * self.quantity
        return total

class ShippingAddress(models.Model):
    user=models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    first_name=models.CharField(max_length=200,null=False)
    last_name=models.CharField(max_length=200,null=False)
    company_name=models.CharField(max_length=200,null=True)
    phone=models.IntegerField(null=False)
    email=models.EmailField()
    address_1=models.CharField(max_length=200,null=False)
    address_2=models.CharField(max_length=200,null=False)
    city=models.CharField(max_length=200,null=False)
    district = models.CharField(max_length=200, null=False)
    state=models.CharField(max_length=200,null=False)
    zipcode=models.CharField(max_length=200,null=False)
    date_added=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name

class BillingAddress(models.Model):
    user=models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    first_name = models.CharField(max_length=200, null=False)
    last_name = models.CharField(max_length=200, null=False)
    company_name = models.CharField(max_length=200, null=True)
    phone = models.IntegerField(null=False)
    email = models.EmailField()
    address_1 = models.CharField(max_length=200, null=False)
    address_2 = models.CharField(max_length=200, null=False)
    city=models.CharField(max_length=200,null=False)
    district = models.CharField(max_length=200, null=False)
    state=models.CharField(max_length=200,null=False)
    zipcode=models.CharField(max_length=200,null=False)
    date_added=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name


class UserAddress(models.Model):
    user=models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    first_name = models.CharField(max_length=200, null=False)
    last_name = models.CharField(max_length=200, null=False)
    company_name = models.CharField(max_length=200, null=True)
    phone = models.IntegerField(null=False)
    email = models.EmailField()
    address_1 = models.CharField(max_length=200, null=False)
    address_2 = models.CharField(max_length=200, null=False)
    city=models.CharField(max_length=200,null=False)
    district = models.CharField(max_length=200, null=False)
    state=models.CharField(max_length=200,null=False)
    zipcode=models.CharField(max_length=200,null=False)
    defaul=models.BooleanField(default=False)
    date_added=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name


class OrderStatus(models.Model):
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    status=models.CharField(max_length=100)
    date_assign=models.DateTimeField(null=True)
    date_added=models.DateTimeField(auto_now_add=True)


class Capacity(models.Model):
    day=models.CharField(max_length=3,unique=True)
    no_deliver=models.IntegerField(default=0)

    def __str__(self):
        return self.day




