import json
import datetime
import calendar
from datetime import date
from admins.models import User
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import *


def cookieCart(request):
    try:
        cart = json.loads(request.COOKIES['cart'])  # getting cart data from cookies
    except:
        cart = {}  # if cookie is empty,then creates empty dictionary
    #print('Cart', cart)
    items = []  # empty item list
    order = {'get_cart_total': 0, 'get_cart_items': 0, 'shipping': False}
    cartItems = order['get_cart_items']

    for i in cart:
        # use try block to prevent items in cart that may have removed from db ,ie when that goes to outof stock,but in cookie,that is in cart
        try:
            cartItems += cart[i]["quantity"]

            product = Product.objects.get(id=i)
            total = (product.price * cart[i]['quantity'])
            #print('Product : ',product)
            order['get_cart_total'] += total
            order['get_cart_items'] += cart[i]['quantity']
            item = {
                'product': {
                    'id': product.id,
                    'name': product.name,
                    'price': product.price,
                    'image1URL': product.image1URL,
                },
                'quantity': cart[i]['quantity'],
                'get_total': total,
            }
            items.append(item)  # adding items from cookies to the empty declared kist

            if product.digital == False:  # for displaying the shipping info inputs,by default it is hidden in the case of non authenticated user
                order['shipping'] = True
        except:
            pass
    #print('cartItems',cartItems)
    #print('order',order)
    #print('items',items)
    return {'cartItems':cartItems,'order':order,'items':items}



def cartData(request):
    if request.user.is_authenticated:
        try:
            user = request.user
            order, created = Order.objects.get_or_create(user=user, complete=False)
            items = order.orderitem_set.all()
            cartItems = order.get_cart_items
        except:    #if the cart is empty then it will show error there is no user
            cartItems = 0
            order = {}
            items = {}
    else:
        #empty cart for non logged in user
        cookieData = cookieCart(request)
        cartItems = cookieData['cartItems']
        order = cookieData['order']
        items = cookieData['items']
    return {'cartItems':cartItems,'order':order,'items':items}



def guestOrder(request,data):
    #print('User is not logged in')

    #print('COOKIES:', request.COOKIES)
    name = data['form']['name']
    email = data['form']['email']

    cookieData = cookieCart(request)
    items = cookieData['items']
    #print(items)

    # for non authenticated user system takes the data from cookie and update the data in db for currespondend user if the user is already registered
    # otherwise ,create a account and update the data in db
    user, created = User.objects.get_or_create(
        email=email,
    )
    user.name = name
    user.save()

    order = Order.objects.create(
        user=user,
        complete=False,
    )

    for item in items:
        product = Product.objects.get(id=item['product']['id'])

        orderItem = OrderItem.objects.create(
            product=product,
            order=order,
            quantity=item['quantity']
        )
    return user,order



def CookieToDb(request,user):   #use to save the cookie datas when looking to checkout without login
    try:
        cart = json.loads(request.COOKIES['cart'])  # getting cart data from cookies
    except:
        cart = {}  # if cookie is empty,then creates empty dictionary
    if cart:
        for i in cart:
            print('product id : ', i, " , quantity :", cart[i]['quantity'])
        # print('Cart', cart)
        for i in cart:
            product = Product.objects.get(id=int(i))
            order, created = Order.objects.get_or_create(user=user, complete=False)

            orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)
            orderItem.quantity = (orderItem.quantity + int(cart[i]['quantity']))
            orderItem.save()

def dispatch_date():
    #today = date.today()
    a=Order.objects.latest('dispatch_date')
    if a.dispatch_date is None:
        today = date.today()
        return today
    else:
        d = datetime.datetime.strptime(str((a.dispatch_date).date()),'%Y-%m-%d').weekday()
        da=calendar.day_name[d]
        if da=='Sunday':
            day='SUN'
        elif da=='Monday':
            day='MON'
        elif da=='Tuesday':
            day='TUE'
        elif da=='Wednesday':
            day='WED'
        elif da=='Thursday':
            day='THU'
        elif da=='Friday':
            day='FRI'
        else:
            day='SAT'
        dat=str((a.dispatch_date).date())
        capacity=Capacity.objects.get(day=day).no_deliver
        count = Order.objects.filter(dispatch_date__startswith=dat).count()
        if count >= capacity:
            new_date= (a.dispatch_date) + datetime.timedelta(days=1)
        else:
            new_date=a.dispatch_date
        return new_date













